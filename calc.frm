VERSION 5.00
Begin VB.Form frmCalc 
   BackColor       =   &H00FFFFFF&
   Caption         =   "Eric's Calculator"
   ClientHeight    =   4215
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3615
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4215
   ScaleWidth      =   3615
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdPeriod 
      Caption         =   "."
      Height          =   495
      Left            =   1920
      TabIndex        =   17
      Top             =   3480
      Width           =   615
   End
   Begin VB.CommandButton cmd0 
      Caption         =   "0"
      Height          =   495
      Left            =   240
      TabIndex        =   16
      Top             =   3480
      Width           =   1455
   End
   Begin VB.CommandButton cmdEqual 
      Caption         =   "="
      Height          =   1215
      Left            =   2760
      TabIndex        =   6
      Top             =   2760
      Width           =   615
   End
   Begin VB.CommandButton cmd3 
      Caption         =   "3"
      Height          =   495
      Left            =   1920
      TabIndex        =   15
      Top             =   2760
      Width           =   615
   End
   Begin VB.CommandButton cmd2 
      Caption         =   "2"
      Height          =   495
      Left            =   1080
      TabIndex        =   14
      Top             =   2760
      Width           =   615
   End
   Begin VB.CommandButton cmd1 
      Caption         =   "1"
      Height          =   495
      Left            =   240
      TabIndex        =   13
      Top             =   2760
      Width           =   615
   End
   Begin VB.CommandButton cmd6 
      Caption         =   "6"
      Height          =   495
      Left            =   1920
      TabIndex        =   12
      Top             =   2040
      Width           =   615
   End
   Begin VB.CommandButton cmd5 
      Caption         =   "5"
      Height          =   495
      Left            =   1080
      TabIndex        =   11
      Top             =   2040
      Width           =   615
   End
   Begin VB.CommandButton cmd4 
      Caption         =   "4"
      Height          =   495
      Left            =   240
      TabIndex        =   10
      Top             =   2040
      Width           =   615
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "+"
      Height          =   1215
      Left            =   2760
      TabIndex        =   5
      Top             =   1320
      Width           =   615
   End
   Begin VB.CommandButton cmd9 
      Caption         =   "9"
      Height          =   495
      Left            =   1920
      TabIndex        =   9
      Top             =   1320
      Width           =   615
   End
   Begin VB.CommandButton cmd8 
      Caption         =   "8"
      Height          =   495
      Left            =   1080
      TabIndex        =   8
      Top             =   1320
      Width           =   615
   End
   Begin VB.CommandButton cmd7 
      Caption         =   "7"
      Height          =   495
      Left            =   240
      TabIndex        =   7
      Top             =   1320
      Width           =   615
   End
   Begin VB.CommandButton cmdSubtract 
      Caption         =   "-"
      Height          =   495
      Left            =   2760
      TabIndex        =   4
      Top             =   600
      Width           =   615
   End
   Begin VB.CommandButton cmdMult 
      Caption         =   "*"
      Height          =   495
      Left            =   1920
      TabIndex        =   3
      Top             =   600
      Width           =   615
   End
   Begin VB.CommandButton cmdDiv 
      Caption         =   "/"
      Height          =   495
      Left            =   1080
      TabIndex        =   2
      Top             =   600
      Width           =   615
   End
   Begin VB.CommandButton cmdClr 
      Caption         =   "CLR"
      Height          =   495
      Left            =   240
      TabIndex        =   1
      Top             =   600
      Width           =   615
   End
   Begin VB.TextBox txtAnswer 
      Height          =   285
      Left            =   360
      TabIndex        =   0
      Text            =   "0"
      Top             =   240
      Width           =   2895
   End
End
Attribute VB_Name = "frmCalc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Written By Eric Cavaliere
' Program Name:           Eric's Calculator
' Project File Name:      calc.vbp
' Source Code File Name:  calc.frm
' Started on:             3/15/2001
' Last Updated on:        3/15/2001

Dim num1$, ans#, sign$

Private Sub cmdClr_Click()
  num1$ = ""
  ans# = 0
  sign$ = ""
  txtAnswer.Text = 0
End Sub

Private Sub cmd5_Click()
  num1$ = num1$ + "5"
  If (sign$ = "=") Then
    sign$ = ""
    ans# = 0
  End If
  txtAnswer.Text = num1$

End Sub

Private Sub cmd6_Click()
  num1$ = num1$ + "6"
  If (sign$ = "=") Then
    sign$ = ""
    ans# = 0
  End If
  txtAnswer.Text = num1$

End Sub

Private Sub cmd1_Click()
  num1$ = num1$ + "1"
  If (sign$ = "=") Then
    sign$ = ""
    ans# = 0
  End If
  txtAnswer.Text = num1$

End Sub

Private Sub cmd2_Click()
  num1$ = num1$ + "2"
  If (sign$ = "=") Then
    sign$ = ""
    ans# = 0
  End If
  txtAnswer.Text = num1$

End Sub

Private Sub cmd3_Click()
  num1$ = num1$ + "3"
  If (sign$ = "=") Then
    sign$ = ""
    ans# = 0
  End If
  txtAnswer.Text = num1$

End Sub

Private Sub cmdEqual_Click()
    If (sign$ = "") Then
      sign$ = "="
      If (num1$ <> "") Then
        ans# = CDbl(num1$)
      Else
        ans# = 0
      End If
      num1$ = ""
    Else ' do previous sign
      If (sign$ = "+") Then
        ans# = CDbl(num1$) + ans#
      End If
      If (sign$ = "-") Then
        ans# = ans# - CDbl(num1$)
      End If
      If (sign$ = "*") Then
        ans# = CDbl(num1$) * ans#
      End If
      If (sign$ = "/") Then
        If (CDbl(num1$) = 0) Then
          num1$ = "1"
        End If
        ans# = ans# / CDbl(num1$)
      End If
      sign$ = "="
      num1$ = ""
    End If
    txtAnswer.Text = ans#

End Sub

Private Sub cmd0_Click()
  num1$ = num1$ + "0"
  If (sign$ = "=") Then
    sign$ = ""
    ans# = 0
  End If
  txtAnswer.Text = num1$
End Sub

Private Sub cmdPeriod_Click()
  num1$ = num1$ + "."
  If (sign$ = "=") Then
    sign$ = ""
    ans# = 0
  End If
  txtAnswer.Text = num1$
End Sub

Private Sub cmdDiv_Click()
    If (sign$ = "") Then
      sign$ = "/"
      If (num1$ <> "") Then
        ans# = CDbl(num1$)
      Else
        ans# = 0
      End If
      num1$ = ""
    Else ' do previous sign
      If (sign$ = "+") Then
        ans# = CDbl(num1$) + ans#
      End If
      If (sign$ = "-") Then
        ans# = ans# - CDbl(num1$)
      End If
      If (sign$ = "*") Then
        ans# = CDbl(num1$) * ans#
      End If
      If (sign$ = "/") Then
        If (CDbl(num1$) = 0) Then
          num1$ = "1"
        End If
        ans# = ans# / CDbl(num1$)
      End If
      sign$ = "/"
      num1$ = ""
    End If
    txtAnswer.Text = ans#

End Sub

Private Sub cmdMult_Click()
    If (sign$ = "") Then
      sign$ = "*"
      If (num1$ <> "") Then
        ans# = CDbl(num1$)
      Else
        ans# = 0
      End If
      num1$ = ""
    Else ' do previous sign
      If (sign$ = "+") Then
        ans# = CDbl(num1$) + ans#
      End If
      If (sign$ = "-") Then
        ans# = ans# - CDbl(num1$)
      End If
      If (sign$ = "*") Then
        ans# = CDbl(num1$) * ans#
      End If
      If (sign$ = "/") Then
        If (CDbl(num1$) = 0) Then
          num1$ = "1"
        End If
        ans# = ans# / CDbl(num1$)
      End If
      sign$ = "*"
      num1$ = ""
    End If
    txtAnswer.Text = ans#

End Sub

Private Sub cmdSubtract_Click()
    If (sign$ = "") Then
      sign$ = "-"
      If (num1$ <> "") Then
        ans# = CDbl(num1$)
      Else
        ans# = 0
      End If
      num1$ = ""
    Else ' do previous sign
      If (sign$ = "+") Then
        ans# = CDbl(num1$) + ans#
      End If
      If (sign$ = "-") Then
        ans# = ans# - CDbl(num1$)
      End If
      If (sign$ = "*") Then
        ans# = CDbl(num1$) * ans#
      End If
      If (sign$ = "/") Then
        If (CDbl(num1$) = 0) Then
          num1$ = "1"
        End If
        ans# = ans# / CDbl(num1$)
      End If
      sign$ = "-"
      num1$ = ""
    End If
    txtAnswer.Text = ans#

End Sub

Private Sub cmd7_Click()
  num1$ = num1$ + "7"
  If (sign$ = "=") Then
    sign$ = ""
    ans# = 0
  End If
  txtAnswer.Text = num1$
End Sub

Private Sub cmd8_Click()
  num1$ = num1$ + "8"
  If (sign$ = "=") Then
    sign$ = ""
    ans# = 0
  End If
  txtAnswer.Text = num1$

End Sub

Private Sub cmd9_Click()
  num1$ = num1$ + "9"
  If (sign$ = "=") Then
    sign$ = ""
    ans# = 0
  End If
  txtAnswer.Text = num1$

End Sub

Private Sub cmdAdd_Click()
    If (sign$ = "") Then
      sign$ = "+"
      If (num1$ <> "") Then
        ans# = CDbl(num1$)
      Else
        ans# = 0
      End If
      num1$ = ""
    Else ' do previous sign
      If (sign$ = "+") Then
        ans# = CDbl(num1$) + ans#
      End If
      If (sign$ = "-") Then
        ans# = ans# - CDbl(num1$)
      End If
      If (sign$ = "*") Then
        ans# = CDbl(num1$) * ans#
      End If
      If (sign$ = "/") Then
        If (CDbl(num1$) = 0) Then
          num1$ = "1"
        End If
        ans# = ans# / CDbl(num1$)
      End If
      sign$ = "+"
      num1$ = ""
    End If
    txtAnswer.Text = ans#

End Sub

Private Sub cmd4_Click()
  num1$ = num1$ + "4"
  If (sign$ = "=") Then
    sign$ = ""
    ans# = 0
  End If
  txtAnswer.Text = num1$
End Sub


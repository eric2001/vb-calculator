# VB Calculator
![VB Calculator](./screenshot.jpg) 

## Description
This is a simple 4-function calculator.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
Install and run the program.  Use the mouse to input numbers and select math functions to perform.

## History
**Version 1.0.0:**
> - Initial Release
> - Released on 15 March 2001
>
> Download: [Version 1.0.0 Setup](/uploads/cd305b6405503fc023f3910807bb75f1/VBCalculatorSetup100.zip) | [Version 1.0.0 Source](/uploads/b425dcdc1d675614153bb5115ead4fa9/VBCalculatorSource100.zip)
